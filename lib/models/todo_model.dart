import '../enum.dart';

class TodoModel {
  String id;
  String? title;
  DateTime? createAt;
  String? content;
  TodoStatus status;

  TodoModel({required this.id, this.title, this.createAt, this.content, required this.status});
}
