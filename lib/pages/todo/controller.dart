import 'package:flutter_test_freelancer/common/common.dart';

import '../../repository/repository.dart';

import '../../models/models.dart';
import 'package:get/get.dart';

import '../../enum.dart';

class TodoController extends GetxController {
  final ITodoRepository todoRepository;

  TodoController({required this.todoRepository});

  final todoList = [
    TodoModel(
      id: '1',
      title: 'Task 1',
      content: 'Taking into consideration the positive response from our customers to the',
      status: TodoStatus.completed,
      createAt: DateTime(2021, 8, 14),
    ),
    TodoModel(
      id: '2',
      title: 'Task 2',
      content: 'Feel free to contact us if you have any questions. Have a great day!',
      status: TodoStatus.incomplete,
      createAt: DateTime(2021, 9, 16),
    ),
    TodoModel(
      id: '3',
      title: 'Task 3',
      content: 'Taking into consideration the positive response from our customers to the',
      status: TodoStatus.completed,
      createAt: DateTime(2021, 10, 25),
    ),
    TodoModel(
      id: '4',
      title: 'Task 4',
      content: 'Taking into consideration the positive response from our customers to the',
      status: TodoStatus.completed,
      createAt: DateTime(2021, 11, 2),
    ),
    TodoModel(
      id: '5',
      title: 'Task 5',
      content: 'Dear Rishi, we have noted on your request to cancel the' '',
      status: TodoStatus.incomplete,
      createAt: DateTime(2021, 12, 6),
    ),
    TodoModel(
      id: '6',
      title: 'Task 6',
      content: 'Taking into consideration the positive response from our customers to the',
      status: TodoStatus.incomplete,
      createAt: DateTime(2021, 12, 22),
    ),
    TodoModel(
      id: '7',
      title: 'Task 7',
      content: 'Feel free to contact us if you have any questions. Have a great day!',
      status: TodoStatus.incomplete,
      createAt: DateTime(2021, 12, 30),
    ),
  ].obs;

  @override
  void onInit() {
    super.onInit();
  }

  void markCompleted({required String id}) {
    final _index = todoList.indexWhere((element) => element.id == id);
    if (_index >= 0) {
      todoList[_index].status = TodoStatus.completed;
      FlutterToast.showToastSuccess(
          message: 'Mark completed ${todoList[_index].title ?? ''} success');
    }
    todoList.refresh();
  }

  void deleteTask({required String id}) {
    todoList.removeWhere((element) => element.id == id);
    FlutterToast.showToastSuccess(message: 'Delete success');
    todoList.refresh();
  }
}
