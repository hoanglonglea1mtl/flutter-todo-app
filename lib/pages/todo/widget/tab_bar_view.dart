import 'package:flutter/material.dart';
import '../../../theme/theme.dart';
import '../../../common/common.dart';
import 'package:get/get.dart';

class TabBarTodoPageWidget extends StatefulWidget {
  final List<String> tabLabels;
  final List<Widget> pages;

  TabBarTodoPageWidget({
    required this.tabLabels,
    required this.pages,
  });

  @override
  _TabBarTodoPageWidgetState createState() => _TabBarTodoPageWidgetState();
}

class _TabBarTodoPageWidgetState extends State<TabBarTodoPageWidget> with SingleTickerProviderStateMixin {
  TabController? _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: widget.pages.length, vsync: this);
  }

  @override
  void dispose() {
    _tabController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisSize: MainAxisSize.min,
      children: [
        _buildTabBar(),
        Expanded(
          child: Padding(
            padding: EdgeInsets.zero,
            child: _buildTabBarView(),
          ),
        ),
      ],
    ).paddingOnly(top: 20, right: 16, left: 16);
  }

  Widget _buildTabBar() {
    final theme = Theme.of(context);
    return Container(
      height: 55,
      padding: const EdgeInsets.all(3),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(38),
      ),
      child: TabBar(
        controller: _tabController,
        unselectedLabelStyle: theme.textTheme.subtitle2!.regular,
        unselectedLabelColor: theme.hintColor,
        labelStyle: theme.textTheme.subtitle2,
        labelColor: Colors.white,
        tabs: widget.tabLabels.map((e) => Tab(text: e)).toList(),
        indicator: BoxDecoration(
          borderRadius: BorderRadius.circular(28),
          color: AssetColors.primary,
        ),
      ),
    );
  }

  Widget _buildTabBarView() {
    return TabBarView(
      controller: _tabController,
      children: widget.pages,
    );
  }
}
