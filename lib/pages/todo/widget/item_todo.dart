import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import '../../../models/models.dart';
import 'package:get/get.dart';
import '../../../common/common.dart';
import '../../../enum.dart';

class ItemTodo extends StatelessWidget {
  final TodoModel model;
  final VoidCallback? onTapMarkCompleted;
  final VoidCallback? onDelete;

  ItemTodo({required this.model, this.onTapMarkCompleted, this.onDelete});

  final _theme = Get.theme;
  @override
  Widget build(BuildContext context) {
    return Slidable(
      key: ValueKey(model.id),
      endActionPane: ActionPane(
        motion: ScrollMotion(),
        children: [
          SlidableAction(
            onPressed: (_) {
              onDelete?.call();
            },
            backgroundColor: Colors.redAccent,
            foregroundColor: Colors.white,
            icon: Icons.delete,
            label: 'Delete',
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                model.title ?? '',
                style: _theme.textTheme.subtitle1,
              ),
              _buildStatus(),
            ],
          ),
          10.verticalSpace,
          Text(
            model.content ?? '',
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            style: _theme.textTheme.subtitle2!.regular,
          ),
          10.verticalSpace,
          _buildDateAndButton(),
        ],
      ).wrapCard,
    );
  }

  Widget _buildStatus() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 3),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100), color: model.status.color.withOpacity(0.1)),
      child: Text(
        model.status.title,
        style: _theme.textTheme.caption!.size(10).textColor(model.status.color).medium,
      ),
    );
  }

  Widget _buildDateAndButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          (model.createAt ?? DateTime.now()).formatDateMMMMddYYY,
          style: _theme.textTheme.caption,
        ),
        if (model.status == TodoStatus.incomplete)
          Text('Mark completed').elevatedButton(onPressed: onTapMarkCompleted).wrapHeight(40),
      ],
    );
  }
}
