import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../enum.dart';
import '../controller.dart';
import '../../../common/common.dart';
import 'widget.dart';

class TabIncompleteView extends GetResponsiveView<TodoController> {
  @override
  Widget phone() {
    return Obx(() {
      final list = controller.todoList.where((p0) => p0.status == TodoStatus.incomplete).toList();
      return ListView.separated(
        padding: const EdgeInsets.symmetric(vertical: 20),
        separatorBuilder: (_, __) => 12.verticalSpace,
        itemBuilder: (_, index) {
          final _model = list[index];
          return ItemTodo(
            model: _model,
            onTapMarkCompleted: () => controller.markCompleted(id: _model.id),
            onDelete: () => controller.deleteTask(id: _model.id),
          );
        },
        itemCount: list.length,
      );
    });
  }

  @override
  Widget desktop() {
    return Obx(() {
      final list = controller.todoList.where((p0) => p0.status == TodoStatus.completed).toList();
      return GridView.count(
        childAspectRatio: 4,
        padding: const EdgeInsets.symmetric(vertical: 20),
        crossAxisCount: 2,
        children: list
            .map(
              (item) => ItemTodo(
                model: item,
                onTapMarkCompleted: () => controller.markCompleted(id: item.id),
                onDelete: () => controller.deleteTask(id: item.id),
              ),
            )
            .toList(),
      );
    });
  }
}
