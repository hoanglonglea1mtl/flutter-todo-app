import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../enum.dart';
import '../controller.dart';
import '../../../common/common.dart';
import 'widget.dart';

class TabCompletedView extends GetResponsiveView<TodoController> {
  @override
  Widget phone() {
    return Obx(() {
      final list = controller.todoList.where((p0) => p0.status == TodoStatus.completed).toList();
      return ListView.separated(
        padding: const EdgeInsets.symmetric(vertical: 20),
        separatorBuilder: (_, __) => 12.verticalSpace,
        itemBuilder: (_, index) {
          final _model = list[index];
          return ItemTodo(
            model: _model,
            onDelete: () => controller.deleteTask(id: _model.id),
          );
        },
        itemCount: list.length,
      );
    });
  }

  @override
  Widget desktop() {
    return Obx(() {
      final list = controller.todoList.where((p0) => p0.status == TodoStatus.completed).toList();
      return GridView.count(
        childAspectRatio: 5,
        padding: const EdgeInsets.symmetric(vertical: 20),
        crossAxisCount: 2,
        children: list
            .map(
              (item) => ItemTodo(
                model: item,
                onDelete: () => controller.deleteTask(id: item.id),
              ),
            )
            .toList(),
      );
    });
  }
}
