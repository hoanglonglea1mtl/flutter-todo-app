import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../common/common.dart';

import 'controller.dart';
import 'widget/widget.dart';

class TodoPage extends GetResponsiveView<TodoController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('TODO'),
      ),
      body: _buildContent(),
    );
  }

  Widget _buildContent() {
    return TabBarTodoPageWidget(tabLabels: [
      'All',
      'Completed',
      'Incomplete'
    ], pages: [
      _buildTabViewAll(),
      TabCompletedView(),
      TabIncompleteView(),
    ]);
  }

  Widget _buildTabViewAll() {
    return Obx(() {
      if (Get.width >= 1200) {
        return GridView.count(
          childAspectRatio: 4,
          padding: const EdgeInsets.symmetric(vertical: 20),
          crossAxisCount: 2,
          children: controller.todoList
              .map(
                (item) => ItemTodo(
                  model: item,
                  onTapMarkCompleted: () => controller.markCompleted(id: item.id),
                  onDelete: () => controller.deleteTask(id: item.id),
                ),
              )
              .toList(),
        );
      }
      return ListView.separated(
        padding: const EdgeInsets.only(bottom: 20),
        itemBuilder: (_, index) => 12.verticalSpace,
        separatorBuilder: (_, index) => ItemTodo(
          model: controller.todoList[index],
          onTapMarkCompleted: () => controller.markCompleted(id: controller.todoList[index].id),
          onDelete: () => controller.deleteTask(id: controller.todoList[index].id),
        ),
        itemCount: controller.todoList.length,
      );
    });
  }
}
