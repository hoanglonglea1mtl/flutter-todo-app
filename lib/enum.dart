import 'package:flutter/material.dart';
import 'theme/theme.dart';

enum TodoStatus { completed, incomplete }

extension TodoStatusExt on TodoStatus {
  Color get color => [AssetColors.colorGreen00C884, AssetColors.colorYellowFD9B37][index];

  String get title => ['Completed', 'Incomplete'][index];
}
