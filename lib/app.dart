import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'repository/repository.dart';
import 'routes/routes.dart';
import 'services/services.dart';
import 'theme/theme.dart';

class FlutterTodoApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle.light.copyWith(statusBarColor: Colors.transparent),
    );

    final initialBinding = BindingsBuilder(() {
      Get.put(APIService());
      Get.lazyPut<ITodoRepository>(() => TodoRepository(service: Get.find()));
    });

    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      theme: FlutterTodoThemeData.themeData,
      darkTheme: FlutterTodoThemeData.themeData,
      getPages: AppPages.pages,
      initialBinding: initialBinding,
      defaultTransition: Transition.native,
      initialRoute: Routes.TODO,
    );
  }
}
