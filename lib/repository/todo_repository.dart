import '../services/services.dart';

abstract class ITodoRepository {
  Future<void> todo();
}

class TodoRepository implements ITodoRepository {
  final APIService service;

  TodoRepository({required this.service});

  @override
  Future<void> todo() {
    // TODO: implement todo
    throw UnimplementedError();
  }
}
